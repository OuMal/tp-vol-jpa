package sopra.vol.dao;

import sopra.vol.Reservation;

public interface IReservationDao extends IDao<Reservation, Long> {
}
