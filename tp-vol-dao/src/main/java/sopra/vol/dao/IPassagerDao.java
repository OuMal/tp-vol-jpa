package sopra.vol.dao;

import sopra.vol.Passager;

public interface IPassagerDao extends IDao<Passager, Long> {
}
