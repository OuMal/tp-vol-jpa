package sopra.vol.dao;

import sopra.vol.Vol;

public interface IVolDao extends IDao<Vol, Long> {
}
