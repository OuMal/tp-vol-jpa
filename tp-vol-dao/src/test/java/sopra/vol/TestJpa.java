package sopra.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import tp.eshop.model.Produit;

public class TestJpa {

	public static void main(String[] args) {
		EntityManagerFactory emf = Singleton.getInstance().getEmf();
		EntityManager em = null;
		EntityTransaction tx = null;
		try {
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			IAeroportDao aeroportDao = Singleton.getInstance().getAeroportDao();
			IVilleDao villeDao = Singleton.getInstance().getVilleDao();
			ICompagnieDao compagnieDao = Singleton.getInstance().getCompagnieDao();
			IPassagerDao passagerDao = Singleton.getInstance().getPassagerDao();
			IReservationDao reservationDao = Singleton.getInstance().getReservationDao();
			IVolDao volDao = Singleton.getInstance().getVolDao();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			Aeroport bdx = new Aeroport();
			bdx.setCode("bdx");
			bdx = aeroportDao.save(bdx);
			
			Aeroport cdg = new Aeroport();
			cdg.setCode("cdg");
			cdg = aeroportDao.save(cdg);
			
			Aeroport nte = new Aeroport();
			nte.setCode("nte");
			nte = aeroportDao.save(nte);
			
			Ville bordeaux = new Ville();
			bordeaux.setNom("Bordeaux");
			bordeaux = villeDao.save(bordeaux);
			
			Ville paris = new Ville();
			paris.setNom("Paris");
			paris = villeDao.save(paris);
			
			Ville nantes = new Ville();
			nantes.setNom("Nantes");
			nantes = villeDao.save(nantes);
			
			Compagnie airfrance = new Compagnie();
			airfrance.setId(01L);
			airfrance.setNomCompagnie("Air France");
			airfrance = compagnieDao.save(airfrance);
			
			Compagnie hop = new Compagnie();
			hop.setId(02L);
			hop.setNomCompagnie("Hop!");
			hop = compagnieDao.save(hop);
			
			Passager benjamin = new Passager();
//			benjamin.setId(01L);
			benjamin.setMail("jabenmin@mail.com");
			benjamin.setTelephone("118218");
			benjamin.setNom("Costille");
			benjamin.setPrenom("Benjamin");
			try {
				benjamin.setDtNaissance(sdf.parse("11/02/1991"));
				benjamin.setDateValiditePI(sdf.parse("25/06/2020"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			benjamin.setNationalite("Française");
			benjamin.setCivilite(Civilite.M);
			benjamin.setTypePI(TypePieceIdentite.CARTE_IDENTITE);	
			benjamin.setNumIdentite("256688552");
			benjamin = passagerDao.save(benjamin);
			
			Reservation reservation1 = new Reservation();
			reservation1.setNumero("255684");
			reservation1.setStatut(true);
			reservation1.setTarif(78.99f);
			reservation1.setTauxTVA(0.20f);
			reservation1.setDateReservation(sdf.parse("22/07/2019"));
			reservation1 = reservationDao.save(reservation1);
			
			Vol parisbordeaux = new Vol();
			parisbordeaux.setNumero("Af250");
			parisbordeaux.setDateDepart(sdf.parse("23/07/2019"));
			parisbordeaux.setDateArrivee(sdf.parse("23/07/2019"));
			parisbordeaux.setOuvert(true);
			parisbordeaux.setNbPlaces(150);
			parisbordeaux.setCompagnie(hop);
			parisbordeaux.setDepart(cdg);
			parisbordeaux.setArrivee(bdx);
			
			parisbordeaux = volDao.save(parisbordeaux);
			
			List<Ville> aeroportville = new ArrayList<Ville>();
			aeroportville.add(bordeaux);
			bdx.setVilles(aeroportville);
			
			bdx = aeroportDao.save(bdx);
			
			List<Ville> aeroportville1 = new ArrayList<Ville>();
			aeroportville1.add(paris);
			cdg.setVilles(aeroportville1);
			
			cdg = aeroportDao.save(cdg);
			Adresse adr = new Adresse();
			benjamin.setPrincipale(adr);
			adr.setVoie("26 rue Félix Sahut");
			adr.setVille("Montpellier");
			adr.setCodePostal("34000");
			adr.setPays("France");
			
			benjamin = passagerDao.save(benjamin);
			Adresse adrPro = new Adresse();
			adrPro.setVille("Besançon");
			
			adrPro.setVoie("157 avenue de la Libération");
			adrPro.setPays("France");
			adrPro.setCodePostal("25000");
			Adresse adrPart = new Adresse();
			adrPart.setVoie("404 rue de l'Internet");
			
			adrPart.setVille("Router");
			adrPart.setPays("France");
			adrPart.setCodePostal("12345");
			Client marcel = new ClientParticulier();
			
			marcel.setMail("marceldu33@flemme.fr");
			marcel.setPrincipale(adrPart);
			marcel = clientDao.save(marcel);
			marcel.setMoyenPaiement(MoyenPaiement.CB);
			marcel.setTelephone("0508080808");
			jeancharles.setFacturation(adrPro);
			Client jeancharles = new ClientPro();
			
			jeancharles.setMail("jc@vd.fr");
			jeancharles = clientDao.save(jeancharles);
			jeancharles.setMoyenPaiement(MoyenPaiement.CHEQUE);
			jeancharles.setTelephone("0585274196");
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
			
			emf.close();
		}
	}

}
