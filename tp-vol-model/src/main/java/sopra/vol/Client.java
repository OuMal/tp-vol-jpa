package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity

public abstract class Client {

	@EmbeddedId
	private Long id;
	@Column(length = 100)
	private String mail;
	@Column(length = 100)
	private String telephone;
	@Column(length = 100)
	@Enumerated(EnumType.ORDINAL)
	private MoyenPaiement moyenPaiement;
	@Embedded
	@Column(length = 100)
	private Adresse principale;
	@Embedded
	@Column(length = 100)
	@AttributeOverrides({
			@AttributeOverride(name = "voie", column = @Column(name = "voie_facturation")),
			@AttributeOverride(name = "complement", column = @Column(name = "complement_facturation")),
			@AttributeOverride(name = "codePostal", column = @Column(name = "codePostal_facturation")),
			@AttributeOverride(name = "ville", column = @Column(name = "ville_facturation")),
			@AttributeOverride(name = "pays", column = @Column(name = "pays_facturation")) })
	private Adresse facturation;
	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
		super();
	}

	public Client(Long id, String mail, String telephone, MoyenPaiement moyenPaiement) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
