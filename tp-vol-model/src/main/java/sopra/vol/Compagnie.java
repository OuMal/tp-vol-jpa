package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Compagnie {
	@Id
	private Long id;
	@Column(length = 100)
	private String nomCompagnie;
	@OneToMany(mappedBy="compagnie", fetch = FetchType.LAZY)
	private List<Vol> vols = new ArrayList<Vol>();

	public Compagnie() {
		super();
	}

	public Compagnie(Long id, String nomCompagnie) {
		super();
		this.id = id;
		this.nomCompagnie = nomCompagnie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public List<Vol> getVols() {
		return vols;
	}

	public void setVols(List<Vol> vols) {
		this.vols = vols;
	}

}
