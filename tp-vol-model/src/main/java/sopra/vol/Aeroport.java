package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.UniqueConstraint;

@Entity
public class Aeroport {
	@Id
	private String code;
	@ManyToMany
	@JoinTable(
	name="aeroport_ville",
	uniqueConstraints=@UniqueConstraint(columnNames = { "AEROPORT_CODE", "VILLE_ID" }),
	joinColumns=@JoinColumn(name="AEROPORT_CODE"),
	inverseJoinColumns=@JoinColumn(name="VILLE_ID"))
	private List<Ville> villes = new ArrayList<Ville>();

	public Aeroport() {
		super();
	}

	public Aeroport(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

}
